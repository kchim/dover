package constants

import "time"

// Global constants, avoid repetition.
const (
	TimeoutRead                     = 30 * time.Second
	TimeoutWrite                    = 30 * time.Second
	RomeBotName                     = "rome-bot"
	RomeBotEmail                    = "automotive-a-team@redhat.com"
	DoverDryRunKey                  = "DOVER_DRY_RUN"
	DoverPort                       = ":8080"
	TestCodeCoverageGatingThreshold = 0.5
	GitlabBaseUrl                   = "https://gitlab.com/api/v4/"
	GitlabPID                       = 28777506
	GitlabTokenEnvKey               = "GITLAB_TOKEN" //nolint:gosec
	GitlabProdBranch                = "main"
	// CAUTION: The folder path below is a temporary path created and deleted recursively inside,
	// Please be cautious if you change the path while debugging or running the tests
	PackageListFileDirectory = "package_list/"
	FileName                 = "c8s-image-manifest.txt"

	//consts referred by githubutil package
	GitHubProdOrg       = "osbuild"
	GitHubDevOrg        = "rome-bot"
	GithubRepo          = "automotive-ci"
	GithubTokenEnvKey   = "GITHUB_TOKEN" //nolint:gosec
	GithubProdBranch    = "main"
	GithubRefPrefix     = "refs/heads/"
	GithubTreeEntryMode = "100644"
)
