package test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/ateam/dover/pkg/router"
)

func TestHealthCheck(t *testing.T) {
	req, err := http.NewRequest("GET", "/health", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(router.HealthCheckHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("unexpected handler return code got %v expected %v", err, http.StatusOK)
	}

	if expected := `{"alive": true}`; rr.Body.String() != expected {
		t.Errorf("health check returned unexpected body, got %v want %v", rr.Body.String(), expected)
	}
}
