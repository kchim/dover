package test

import (
	"bytes"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/ateam/dover/pkg/router"
)

func TestSubmitPackageListHandler(t *testing.T) {
	os.Setenv("GITHUB_TOKEN", "Test")
	os.Setenv("DOVER_DRY_RUN", "true")
	getwd, err := os.Getwd()
	if err != nil {
		return
	}

	file, err := os.Open(getwd + "/files/c8s-image-manifest.txt")
	if err != nil {
		log.Fatal(err)
	}

	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			return
		}
	}(file)

	params := map[string]string{"branch": "dover-test-branch", "commitMessage": "test"}

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", filepath.Base(file.Name()))

	if err != nil {
		t.Fatal(err)
	}

	_, err = io.Copy(part, file)
	if err != nil {
		return
	}

	for key, val := range params {
		_ = writer.WriteField(key, val)
	}

	err = writer.Close()
	if err != nil {
		t.Fatal(err)
	}

	log.Print(body)

	req, err := http.NewRequest("POST", "/submit/package_list", body)
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("Content-Type", writer.FormDataContentType())
	req.Header.Set("boundary", "-------------------------------dover")

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(router.SubmitPackageListHandlerGithub)

	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusOK {
		t.Errorf("unexpected handler return code got %v expected %v", rr.Code, http.StatusOK)
	}
}
