package test

import (
	"log"
	"net/http"
	"os"
	"testing"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/ateam/dover/pkg/gitlabutil"
)

type GitlabClientMock struct {
	Branch gitlabutil.Branch
}
type GitlabMock struct {
	Client *GitlabClientMock
}

func (g *GitlabMock) CreateClient() error {
	return error(nil)
}
func (g *GitlabMock) SetBaseURL(url string) error {
	return nil
}
func (g *GitlabMock) CreateBranch(repoattr *gitlabutil.RepoAttr, branchAttr *gitlabutil.BranchAttr) (*gitlabutil.Branch, *gitlabutil.Response, error) {
	return &gitlabutil.Branch{Name: "Test"}, &gitlabutil.Response{Response: &http.Response{}}, error(nil)
}
func (g *GitlabMock) CreateCommit(repoattr *gitlabutil.RepoAttr, commitAttr *gitlabutil.CommitAttr, branchAttr *gitlabutil.BranchAttr) (*gitlabutil.Commit,
	*gitlabutil.Response, error) {
	return &gitlabutil.Commit{ID: "123"}, &gitlabutil.Response{Response: &http.Response{}}, error(nil)
}
func (g *GitlabMock) DeleteBranch(repoattr *gitlabutil.RepoAttr, branchAttr *gitlabutil.BranchAttr) (*gitlabutil.Response, error) {
	return &gitlabutil.Response{Response: &http.Response{}}, error(nil)
}
func (g *GitlabMock) CreateMergeRequest(repoattr *gitlabutil.RepoAttr, mergeAttr *gitlabutil.MergeAttr, branchAttr *gitlabutil.BranchAttr) (*gitlabutil.MergeRequest,
	*gitlabutil.Response, error) {
	return &gitlabutil.MergeRequest{ID: 123}, &gitlabutil.Response{Response: &http.Response{}}, error(nil)
}

func SetUpGitlabMock() *gitlabutil.GitClientAttr {
	var gitlabMock gitlabutil.GitForge = &GitlabMock{Client: &GitlabClientMock{Branch: gitlabutil.Branch{Name: "test"}}}
	return gitlabutil.NewGitClientAttr(&gitlabMock)
}
func SetUp() *gitlabutil.GitClientAttr {
	var gitlab gitlabutil.GitForge = &gitlabutil.Gitlab{Client: &gitlab.Client{}}
	return gitlabutil.NewGitClientAttr(&gitlab)
}

func TestCreateClient(t *testing.T) {
	gitClient := SetUp()
	if gitClient == nil {
		t.Errorf("gitlabutils.NewGitClientAttr error")
	} else {
		client := gitClient.Client.CreateClient()
		if client == nil {
			t.Errorf("gitlabutils.Cleint.CreateClient error")
		}
	}
}
func TestSetBaseURLWrapper(t *testing.T) {
	gitClient := SetUp()
	if gitClient == nil {
		t.Errorf("gitlabutils.NewGitClientAttr error")
	} else {
		if gitClient.Client.SetBaseURL("") != nil {
			t.Errorf("gitlabutils.Cleint.CreateClient error")
		}
	}
}
func TestCreateBranch(t *testing.T) {
	gitclient := SetUpGitlabMock()

	if gitclient == nil {
		t.Errorf("gitlabutils.NewGitClientAttr error")
	}

	br, err := gitclient.CreateBranchWrapper(&gitlabutil.RepoAttr{ProjectID: 1234}, &gitlabutil.BranchAttr{SourceBranch: "test", TargetBranch: "main"})
	if err != nil {
		t.Errorf("gitlabutils.CreateBranchWrapper error")
	}
	if br != nil && br.Name != "Test" {
		t.Errorf("gitlabutils.CreateBranchWrapper error")
	}
}
func TestCreateCommit(t *testing.T) {
	gitclient := SetUpGitlabMock()

	if gitclient == nil {
		t.Errorf("gitlabutils.NewGitClientAttr error")
	}
	getwd, err := os.Getwd()
	if err != nil {
		return
	}
	file, err := os.Open(getwd + "/files/c8s-image-manifest.txt")
	if err != nil {
		log.Fatal(err)
	}
	br, err := gitclient.CreateCommitWrapper(&gitlabutil.RepoAttr{ProjectID: 1234}, &gitlabutil.CommitAttr{AuthorName: "A-team",
		AuthorEmail: "A-team@redhat.com", CommitMsg: "Test String",
		SourceFile: file.Name()}, &gitlabutil.BranchAttr{SourceBranch: "test", TargetBranch: "main"})
	if br == nil || err != nil {
		t.Errorf("gitlabutils.CreateCommitWrapper error")
	}
}

func TestDeleteBranch(t *testing.T) {
	gitclient := SetUpGitlabMock()

	if gitclient == nil {
		t.Errorf("gitlabutils.NewGitClientAttr error")
	}

	br, err := gitclient.CreateCommitWrapper(&gitlabutil.RepoAttr{ProjectID: 1234}, &gitlabutil.CommitAttr{AuthorName: "A-team",
		AuthorEmail: "A-team@redhat.com", CommitMsg: "Test String",
		SourceFile: "/files/c8s-image-manifest.txt"}, &gitlabutil.BranchAttr{SourceBranch: "test", TargetBranch: "main"})
	if br != nil || err != nil {
		t.Errorf("gitlabutils.CreateCommitWrapper error")
	}
}
func TestCreateMergeRequest(t *testing.T) {
	gitclient := SetUpGitlabMock()

	if gitclient == nil {
		t.Errorf("gitlabutils.NewGitClientAttr error")
	}

	br, err := gitclient.CreateMRWrapper(&gitlabutil.RepoAttr{ProjectID: 1234}, &gitlabutil.MergeAttr{Title: "Test MR",
		Description: "test commit message"}, &gitlabutil.BranchAttr{SourceBranch: "test", TargetBranch: "main"})
	if br == nil || err != nil {
		t.Errorf("gitlabutils.CreateMRWrapper error")
	}
}
