package gitlabutil

import (
	"errors"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/ateam/dover/constants"
)

type Gitlab struct {
	Client *gitlab.Client
}

func (g *Gitlab) SetBaseURL(url string) error {
	return g.Client.SetBaseURL(url)
}

func (g *GitClientAttr) SetBaseURLWrapper(url string) error {
	return g.Client.SetBaseURL(url)
}

func (g *Gitlab) CreateClient() error {
	err := error(nil)

	client := http.Client{Timeout: constants.TimeoutRead}

	GitlabToken := os.Getenv(constants.GitlabTokenEnvKey)

	if GitlabToken == "" {
		return errors.New("required Env Var GITLAB_TOKEN not found")
	}

	g.Client = gitlab.NewOAuthClient(&client, GitlabToken)
	return err
}

func (g *GitClientAttr) CreateBranchWrapper(repoAttr *RepoAttr, branchAttr *BranchAttr) (*Branch, error) {

	branch, response, err := g.Client.CreateBranch(repoAttr, branchAttr)

	if err != nil {
		g.Serialize(response, true)
	}

	return branch, err
}

func (g *Gitlab) CreateBranch(repoAttr *RepoAttr, branchAttr *BranchAttr) (*Branch, *Response, error) {

	br, res, err := g.Client.Branches.CreateBranch(repoAttr.ProjectID, &gitlab.CreateBranchOptions{
		Branch: &branchAttr.SourceBranch,
		Ref:    &branchAttr.TargetBranch,
	})
	var branch Branch
	var response Response
	if br != nil {
		branch.Name = br.Name
	}
	if res != nil {
		response.Response = res.Response
	}

	return &branch, &response, err
}
func (g *Gitlab) DeleteBranch(repoAttr *RepoAttr, branchAttr *BranchAttr) (*Response, error) {

	response, err := g.Client.Branches.DeleteBranch(repoAttr.ProjectID, branchAttr.SourceBranch)
	return &Response{Response: response.Response}, err
}

// TODO
// 1. a conatainer with structs of action type and file
// 2. handle different actions besides updates
func (g *Gitlab) CreateCommit(repoAttr *RepoAttr, commitAttr *CommitAttr, branchAttr *BranchAttr) (*Commit, *Response, error) {

	com, res, err := g.Client.Commits.CreateCommit(repoAttr.ProjectID, &gitlab.CreateCommitOptions{
		Branch:        &branchAttr.SourceBranch,
		CommitMessage: &commitAttr.CommitMsg,
		Actions: []*gitlab.CommitAction{{
			Action:   gitlab.FileUpdate,
			FilePath: commitAttr.SourceFile,
			Content:  string(commitAttr.content),
		}},
		AuthorEmail: &commitAttr.AuthorEmail,
		AuthorName:  &commitAttr.AuthorName})

	var commit Commit
	var response Response

	if com != nil {
		commit.ID = com.ID
	}
	if res != nil {
		response.Response = res.Response
	}
	return &Commit{ID: commit.ID}, &Response{Response: response.Response}, err
}

func (g *GitClientAttr) CreateCommitWrapper(repoAttr *RepoAttr, commitAttr *CommitAttr,
	branchAttr *BranchAttr) (*Commit, error) {
	var commit *Commit
	var response *Response
	bytes, err := ioutil.ReadFile(commitAttr.SourceFile)
	commitAttr.content = bytes

	// successful return of file content read returns nil
	if err == nil {
		commit, response, err = g.Client.CreateCommit(repoAttr, commitAttr, branchAttr)

		if err != nil {
			g.Serialize(response, true)
		}
	} else {
		response, err = g.Client.DeleteBranch(repoAttr, branchAttr)
		if err != nil {
			g.Serialize(response, true)
		}
	}

	return commit, err
}

func (g *Gitlab) CreateMergeRequest(repoAttr *RepoAttr, mergeAttr *MergeAttr,
	branchAttr *BranchAttr) (*MergeRequest, *Response, error) {

	mergeRequestOptions := gitlab.CreateMergeRequestOptions{
		Title:        &mergeAttr.Title,
		Description:  &mergeAttr.Description,
		SourceBranch: &branchAttr.SourceBranch,
		TargetBranch: &branchAttr.TargetBranch,
	}

	mr, res, err := g.Client.MergeRequests.CreateMergeRequest(repoAttr.ProjectID, &mergeRequestOptions)
	var mergeReq MergeRequest
	var response Response
	if mr != nil {
		mergeReq.ID = mr.ID
	}
	if res != nil {
		response.Response = res.Response
	}
	return &mergeReq, &response, err
}
func (g *GitClientAttr) CreateMRWrapper(repoAttr *RepoAttr, mergeAttr *MergeAttr, branchAttr *BranchAttr) (*MergeRequest, error) {
	mr, response, err := g.Client.CreateMergeRequest(repoAttr, mergeAttr, branchAttr)

	if err != nil {
		g.Serialize(response, true)
	}

	return &MergeRequest{ID: mr.ID}, err
}
