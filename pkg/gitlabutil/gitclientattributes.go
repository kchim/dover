package gitlabutil

import (
	"net/http"
	"os"

	"github.com/apsdehal/go-logger"
	"github.com/hprose/hprose-go"
)

// Minimal MergeRequest information
// TODO: Revisit to put the manadatory attributes
type MergeRequest struct {
	ID        int    `json:"id"`
	ProjectID int    `json:"project_id"`
	State     string `json:"state"`
}

// Minimal Commit information
// TODO: Revisit to put the manadatory attributes
type Commit struct {
	ID string `json:"id"`
}

// Minimal Branch information
// TODO: Revisit to put the manadatory attributes
type Branch struct {
	Name string `json:"name"`
}

// Minimal Response information
// TODO: Revisit to put the manadatory attributes
type Response struct {
	*http.Response
}

type RepoAttr struct {
	ProjectID int
}
type BranchAttr struct {
	SourceBranch string
	TargetBranch string
}
type CommitAttr struct {
	AuthorName  string
	AuthorEmail string
	CommitMsg   string
	SourceFile  string
	content     []byte
}
type MergeAttr struct {
	Title       string
	Description string
	BranchAttr  *BranchAttr
}
type GitForge interface {
	SetBaseURL(string) error
	CreateClient() error
	CreateBranch(repoAttr *RepoAttr, branchAttr *BranchAttr) (*Branch, *Response, error)
	CreateCommit(repoAttr *RepoAttr, commitAttr *CommitAttr, branchAttr *BranchAttr) (*Commit, *Response, error)
	DeleteBranch(repoAttr *RepoAttr, BranchAttr *BranchAttr) (*Response, error)
	CreateMergeRequest(repoAttr *RepoAttr, mergeAttr *MergeAttr, branchAttr *BranchAttr) (*MergeRequest, *Response, error)
}
type GitClientAttr struct {
	Client GitForge
	Log    *logger.Logger
}

func (g *GitClientAttr) Serialize(r interface{}, simple bool) {
	res, err := hprose.Serialize(r, simple)

	if err != nil && res != nil {
		g.Log.Infof("Data Serialized: %s", res)
	} else {
		g.Log.Warningf("Serialization problem: %v", err)
	}
}

func NewGitClientAttr(Client *GitForge) *GitClientAttr {
	log, err := logger.New("GitClientAttr", 1, os.Stdout)
	var gitClientAttr *GitClientAttr

	if log != nil && err == nil {
		gitClientAttr = new(GitClientAttr)
		gitClientAttr.Client = *Client
		gitClientAttr.Log = log
	}
	return gitClientAttr
}
