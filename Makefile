.PHONY: code/check dover test ci-test deps image/build fmt vet stage/up prod/up


GITLAB_TOKEN ?= "empty"
DOVER_DRY_RUN = "True"

code/check: deps fmt vet lint

fmt:
	go fmt `go list ./... | grep -v /vendor/`

vet:
	go vet `go list ./... | grep -v /vendor/`

lint:
	golangci-lint run ./...

test: code/check
	go clean -testcache
	go test `go list ./... | grep -v /vendor/` -v -coverpkg=./pkg/gitlabutil,./pkg/router  --coverprofile=testCoverage.txt

ci-test:
	go clean -testcache
	go test `go list ./... | grep -v /vendor/` -v -coverpkg=./pkg/gitlabutil,./pkg/router  --coverprofile=testCoverage.txt

deps:
	go mod tidy
	go mod vendor

image/build: deps
	podman build -t dover .

image/run: image/build
	podman run --rm -d --env GITLAB_TOKEN=${GITLAB_TOKEN} --network="host" -t localhost/dover

image/run/dev: image/build
	podman run --rm -d --env GITLAB_TOKEN=${GITLAB_TOKEN} --network="host" -t localhost/dover

stage/up:
	ansible-playbook deployment/site.yml -e env=stage

prod/up:
	ansible-playbook deployment/site.yml -e env=prod
