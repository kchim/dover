# Dover
A microservice to ferry files to GitLab and create Merge Requests for them.

# Getting started

*dover* is a service that listens to the requests from any services to creates merge requests against a GitLab repository - [manifests](https://gitlab.com/redhat/edge/ci-cd/manifests). The merge request proposes the changes to the package list file. in[rome](https://gitlab.com/redhat/edge/ci-cd/a-team/rome) is one of the services that sends such a request to *dover*.
## API endpoints

### GET `/health` 
    RETURNS: 
        `{"alive": true}`

### POST `/submit/package_list`  
    PARAMS:
        {  
            "branch": "<branchname>",
            "commitMessage": "<commitmessage>",
            "data": "<data>"
        }

## Installation
### Using container

**Prerequisite**

- Install container - software packager. You can use any container of your choice ([docker](https://www.docker.com/) or [podman](https://podman.io/)) to build the service image and get it up and running. All the necessary rules are already defined in the *Makefile*. 
- Install RabbitMQ and start the service. Follow the [documentation](https://www.rabbitmq.com/download.html).

**To start *dover* in the development environment**

- Provide environment variables

```sh
export GITLAB_TOKEN=<gitlab access token with admin:repo privileges>

# ONLY SET THIS ENV VAR FOR TESTING
export DOVER_DRY_RUN=true
```

- Navigate to the project root directory and run `make image/run/dev`. This command will create the image called *dover* and start the container.

### Without container

**Prerequisite**
- Download and install [Go](https://golang.org/doc/install) version 1.16 or above
- Run the following
```
go mod download && go mod vendor
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o dover main.go
```

**To start *dover* in the development environment**

- Provide environment variables

```sh
export GITLAB_TOKEN=<gitlab access token with admin:repo privileges>

# ONLY SET THIS ENV VAR FOR TESTING
export DOVER_DRY_RUN=true
```

- Navigate to project root directory and run `/usr/local/bin/dover`
# How to contribute

- Fork the project
- Clone your fork
- Add upstream remote: `git remote add upstream https://gitlab.com/redhat/edge/ci-cd/a-team/dover.git`
- Checkout new branch from *main* branch: `git checkout -b <branch-name>`
- Commit and push your branch
```
git add <file-name>
git commit -m <commit-messages>
git push origin <branch-name>
```
- Create a merge request against the upstream repository and provide a summary of what it is about
- Assign reviewers
- Resolve reviews if any
- Wait for approval(s) and your branch will be merged

### Code guidelines

- Provide unit test(s)
- For code format, *isort*, *flake8* and *black* are applied.
- Before pushing your code, make sure that linter and unit tests pass.
```
#Inside the project root directory, run the following commands:

make lint
make test
```

# Report issues

If you find any issue/bug, please don't hesitate to create one via this [link](https://gitlab.com/redhat/edge/ci-cd/a-team/dover/-/issues/new).
